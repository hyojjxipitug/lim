extends Node2D

var click_count := 0
var board_clear := true
var state := [] setget set_state
var _remaining_fmt := "Clicks before explosion: {clicks}"

var click_history := []
var history_pointer := 0

var score_max := 2000
var click_max := Singleton.max_clicks

onready var balls := [
	$balls/ball1, 
	$balls/ball2,
	$balls/ball3, 
	$balls/ball4,
	$balls/ball5, 
	$balls/ball6,
	$balls/ball7, 
	$balls/ball8,
	$balls/ball9, 
	$balls/ball10,
	$balls/ball11, 
	$balls/ball12,
	$balls/ball13, 
	$balls/ball14,
	$balls/ball15, 
	$balls/ball16,
]

func set_state(new_value: Array) -> void:
	state = new_value
	refresh_board_display()

func _ready() -> void:
	print("coucou ", Singleton.difficulty)
	randomize()
	for _i in range(16):
		state.append(false)
	set_state(random_state(Singleton.difficulty))
	$remaining.text = _remaining_fmt.format({"clicks": Singleton.max_clicks - click_count})

func _on_click_detection_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event is InputEventMouseButton:
		var e = event as InputEventMouseButton
		if not e.pressed:
			var local_pos = $click_detection.to_local(e.position)
			# print("click detected, position: ", local_pos)
			var cell_location = Vector2(int(local_pos.x / 128), int(local_pos.y / 128))
			cell_clicked(cell_location)
			
static func loc_to_idx(loc: Vector2) -> int:
	return (4 * int(loc.y) + int(loc.x))

static func get_neighbours(loc: Vector2) -> Array:
	var neighbours := []
	var deltas = [Vector2(0, -1), Vector2(0, 1), Vector2(-1, 0), Vector2(1, 0)]
	for d in deltas:
		var cand = loc + d
		if 0 <= cand.x and cand.x <= 3 and 0 <= cand.y and cand.y <= 3:
			neighbours.append(cand)
	return neighbours

func switch_cell_state(location: Vector2) -> void:
	print("cell coordinates: ", location)
	for n in get_neighbours(location):
		var idx = loc_to_idx(n)
		self.state[idx] = !self.state[idx]
	refresh_board_display()

func cell_clicked(location: Vector2) -> void:
	switch_cell_state(location)
	push_click(location)
# warning-ignore:integer_division
	Singleton.score = (click_max - click_count) * score_max / click_max
	if self.board_clear and click_count > 0:
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://scenes/GameOver.tscn")
	if click_count > click_max:
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://scenes/BlackGameOver.tscn")
		

func refresh_board_display() -> void:
	self.board_clear = true
	for idx in range(16):
		if self.state[idx]: 
			self.balls[idx].ball_visible = true
			self.board_clear = false
		else:
			self.balls[idx].ball_visible = false


static func random_state(depth: int) -> Array:
	var s = []
	var selected = []
	for _idx in range(16):
		s.append(false)
	for _count in range(depth):
		var in_selected := true
		var location := Vector2.ZERO
		while in_selected:
			location.x = randi() % 4
			location.y = randi() % 4
			if not location in selected:
				in_selected = false
				selected.append(location)
			else:
				print("skip dup")
		for n in get_neighbours(location):
			var idx = loc_to_idx(n)
			s[idx] = !s[idx]
	return s

func push_click(location: Vector2) -> void:
	while self.history_pointer < self.click_history.size():
		self.click_history.pop_back()
	self.click_history.append(location)
	self.history_pointer += 1
	self.click_count += 1
	$Dynamite.progress = float(click_count) / click_max
	$remaining.text = _remaining_fmt.format({"clicks": Singleton.max_clicks - click_count})


func undo_click() -> void:
	print("--- begin undo ---")
	print("pointer: %s, history size: %s" % [self.history_pointer, self.click_history.size()])
	if self.history_pointer > 0:
		switch_cell_state(self.click_history[self.history_pointer-1])
		self.history_pointer = self.history_pointer - 1
	print("pointer: %s, history size: %s" % [self.history_pointer, self.click_history.size()])
	print("--- end undo ---")
	
func redo_click() -> void:
	print("--- begin redo ---")
	print("pointer: %s, history size: %s" % [self.history_pointer, self.click_history.size()])
	if self.history_pointer < self.click_history.size():
		self.history_pointer = self.history_pointer + 1
		switch_cell_state(self.click_history[self.history_pointer-1])
	print("pointer: %s, history size: %s" % [self.history_pointer, self.click_history.size()])
	print("--- end redo ---")


# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_undo_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton:
		var e = event as InputEventMouseButton
		if e.pressed:
			undo_click()


# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_redo_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton:
		var e = event as InputEventMouseButton
		if e.pressed:
			redo_click()

# warning-ignore:unused_argument
func _process(delta: float) -> void:
	for b in balls:
		b.highlight = false
	var location = get_viewport().get_mouse_position()
	var local_pos = $click_detection.to_local(location)
	var loc = Vector2(int(local_pos.x / 128), int(local_pos.y / 128))
	if 0 <= loc.x and loc.x <= 3 and 0 <= loc.y and loc.y <= 3:
		var neighbours = get_neighbours(loc)
		for n in neighbours:
			balls[loc_to_idx(n)].highlight = true

