tool
extends Node2D

export(float) var progress := 0.0 setget set_progress

func set_progress(new_value: float) -> void:
	progress = new_value
	# 100% => tip_group.y = 20
	# 0% => tip_group.y = -540
	var delta = 560
	var new_y = 20 - ((1 - progress) * delta)
	$tip_group.position.y = new_y

