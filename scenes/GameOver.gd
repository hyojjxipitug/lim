extends Control


func _ready() -> void:
	display_score()

func display_score() -> void:
	$CenterContainer/VBoxContainer/Label.text = """
	Well done!
	Final score: %s
	""" % Singleton.score


func _on_Button_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/StartMenu.tscn")
