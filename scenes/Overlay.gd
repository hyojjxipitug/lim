extends Control

signal undo
signal redo

var _remaining_fmt := "Clicks before explosion: {clicks}"
onready var scoreboard := $CenterContainer/HBoxContainer/VBoxContainer/remaining

func _on_board_click_count(count) -> void:
	scoreboard.text = _remaining_fmt.format({"clicks": Singleton.max_clicks - count})


func _on_undo_pressed() -> void:
	emit_signal("undo")


func _on_redo_pressed() -> void:
	emit_signal("redo")
