extends Control



func _ready() -> void:
	print("ready startmenu")
	$CenterContainer/VBoxContainer/OptionButton.add_item("Easy", 0)
	$CenterContainer/VBoxContainer/OptionButton.add_item("Moderate", 1)
	$CenterContainer/VBoxContainer/OptionButton.add_item("Difficult", 2)
	$CenterContainer/VBoxContainer/OptionButton.select(Singleton.difficulty_idx)

func _on_Button_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/board.tscn")




func _on_OptionButton_item_selected(index: int) -> void:
	Singleton.difficulty_idx = index
	print("idx ", index)
	if index == 0:
		Singleton.max_clicks = 10
		Singleton.difficulty = 3
	elif index == 1:
		Singleton.max_clicks = 20
		Singleton.difficulty = 5
	elif index == 2:
		Singleton.max_clicks = 10
		Singleton.difficulty = 10
